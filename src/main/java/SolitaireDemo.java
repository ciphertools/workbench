/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
import tools.cipher.ciphers.solitaire.DeckParse;
import tools.cipher.ciphers.solitaire.SolitaireSolver;
import tools.cipher.lib.language.Languages;
import tools.cipher.solve.SolitaireAttack;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class SolitaireDemo {

    public static void main(String[] args) {
        Languages.ENGLISH.loadNGramData();

        String cipherText;
        try {
            cipherText = Files.readString(Path.of(Thread.currentThread().getContextClassLoader().getResource("2015-8B.txt").toURI()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        String deck = """
            KH,9H,8S,4C,5C,3S,\
            4D,KS,7D,KC,A,3D,\
            4H,AS,QH,8H,4S,AD,\
            2S,7C,AH,5S,AC,6C,\
            7H,2D,B,TH,5D,JD,\
            3C,9C,QS,JH,TD,*,\
            *,*,*,*,*,*,\
            *,QD,*,*,*,*,\
            *,*,6H,*,3H,*\
            """;
        System.out.println(deck);
        SolitaireSolver.startCompleteAttack(cipherText, 7, 256 * 5, new DeckParse(deck), System.out, 0);
    }
}
